import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import CalendarPage from './components/CalendarPage/CalendarPage';
import Login from './components/Login/Login';
import Register from './components/Register/Register';

const App = () => {
  return(
    <div>
      <BrowserRouter>
        <div>
          <Routes>
            <Route exact path="/" element={<CalendarPage/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/login" element={<Login/>}/>
          </Routes>
          {/*<CalendarPage/>*/}
        </div>
      </BrowserRouter>
    </div>

  )
}

export default App;
