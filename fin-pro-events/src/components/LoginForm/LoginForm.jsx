import React from 'react';
import styles from "./LoginForm.module.css";

const LoginForm = () => {
    return(
        <div className="register-container">
            <form className="register-form">
                <fieldset className={styles.fieldset}>
                    <fieldset className={styles.fieldset}>
                        <label className={styles.label}>Email</label>
                        <div />
                        <input type='Email' className={styles.input} />
                    </fieldset>

                    <fieldset className={styles.fieldset}>
                        <label className={styles.label}>Password</label>
                        <div />
                        <input type='password' className={styles.input} />
                    </fieldset>
                    <div>
                        <button className={styles.button} align='center'>Войти</button>
                    </div>
                </fieldset>
            </form>
        </div>
    )
}

export default LoginForm;