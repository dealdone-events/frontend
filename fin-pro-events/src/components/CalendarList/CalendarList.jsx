import React from 'react';
import styles from "./CalendarList.module.css";
import Event from "../Event/Event";

const CalendarList = (props) => {
    return(
        <div className={styles.calendarlistcontainer}>
            <div className={styles.listofevents}>
                {
                    props.list.map(event => {
                        return(
                            <Event 
                                prioritet={event.prioritet} 
                                name={event.name} 
                                attributes={event.attributes} 
                                date={event.date}/>
                        );
                    })
                }
            </div>
        </div>
    )
}

export default CalendarList;