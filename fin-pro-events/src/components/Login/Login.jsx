import React from "react";
import styles from "./Login.module.css";
import LoginForm from "../LoginForm/LoginForm";
import GoogleButton from "react-google-button";
import { Link } from "react-router-dom";

const Login = () => {
    return(
        <div>
            <div className={styles.div}>
            <h1 align='center'>Войти</h1>

            <LoginForm />

            <fieldset className={styles.fieldset}>
                <input type='checkbox' />
                <label>Не оставаться в системе</label>
            </fieldset>

            <fieldset className={styles.fieldset}>
                <Link to="/calendar">
                    Войти позже
                </Link>
            </fieldset>

            <fieldset className={styles.fieldset}>
                <label>Нет аккаунта? </label>
                <Link to="/register">
                    Зарегистрируйтесь!
                </Link>

            </fieldset>

            <label className={styles.label}>or register with email</label>
            <GoogleButton
                label='Войти'
            />
        </div>
        </div>
    )
}

export default Login;