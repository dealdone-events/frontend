import React from "react";
import s from "./RegisterForm.module.css"

const RegisterForm = () => {
    return (
        <div className="register-container">
            <form className="register-form">
                <fieldset className={s.fieldset}>
                    <fieldset className={s.fieldset}>
                        <label className={s.label}>Name</label>
                        <div />
                        <input type='text' className={s.input} />
                    </fieldset>

                    <fieldset className={s.fieldset}>
                        <label className={s.label}>Email</label>
                        <div />
                        <input type='Email' className={s.input} />
                    </fieldset>

                    <fieldset className={s.fieldset}>
                        <label className={s.label}>Password</label>
                        <div />
                        <input type='password' className={s.input} />
                    </fieldset>

                    <fieldset className={s.fieldset}>
                        <label className={s.label}>Password again</label>
                        <div />
                        <input type='password' className={s.input} />
                    </fieldset>

                    <div>
                        <button className={s.button} align='center'>Register</button>
                    </div>
                </fieldset>
            </form>
        </div>
    )
}

export default RegisterForm;