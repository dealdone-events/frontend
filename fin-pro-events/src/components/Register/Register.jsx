import React from "react";
import classes from './Register.module.css';
import GoogleButton from "react-google-button";
import RegisterForm from "../RegisterForm/RegisterForm"
import { Link } from "react-router-dom";


const Register = () => {
    const onRegisterSubmit = data => data;

    return (
        <div className={classes.div}>
            <h1 align='center'>Registration</h1>

            <RegisterForm />

            <fieldset className={classes.fieldset}>
                <input type='checkbox' />
                <label> Не оставаться в системе</label>
            </fieldset>

            <fieldset className={classes.fieldset}>
                <Link to="/calendar">
                    Зарегистрироваться позже
                </Link>
            </fieldset>

            <fieldset className={classes.fieldset}>
                <label>Уже есть аккаунт? </label>
                <Link to="/login">
                    Войти
                </Link>
            </fieldset>

            <label className={classes.label}>or register with email</label>
            <GoogleButton
                label='Зарегистрироваться'
            />
        </div>
    )
}


export default Register;