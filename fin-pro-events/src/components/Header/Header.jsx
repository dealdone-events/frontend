import React from "react";
import SearchInput from "../SearchInput/SearchInput";
import styles from "./Header.module.css";

const Header = () => {
    return(
        <div>
            <header className={styles.header}>
                <div className={styles.headerwrap}>
                    <label className={styles.logo}>FinPro Events</label>
                    <SearchInput className={styles.searchinput}/>
                    <button className={styles.notificationbutton} ></button>
                    <button className={styles.profilebutton}>Профиль</button>
                </div>
            </header>
        </div>
    )
}

export default Header;