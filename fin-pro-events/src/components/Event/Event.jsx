import React from "react";
import styles from "./Event.module.css";

const Event = (props) => {
    return(
        <div className={styles.eventcontainer}>
            <ul>
                <li className={styles.enuminfo}>{props.prioritet}</li>
                <li className={styles.enuminfo}>{props.name}</li>
                <li className={styles.enuminfo}>{props.attributes}</li>
            </ul>
            <label>{props.date}</label>
            <button>add</button>
        </div>
    )
}

export default Event;