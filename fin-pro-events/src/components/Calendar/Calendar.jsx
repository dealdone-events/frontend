import React from "react";
import SearchCalendar from "../SearchCalendar/SearchCalendar";
import CalendarList from "../CalendarList/CalendarList";

const Calendar = () => {
    return(
        <div>
            <SearchCalendar/>
            <CalendarList list={
                [
                    {
                        prioritet: "Medium",
                        name: "Tinkoff",
                        attributes: "just attributes",
                        date: "05-05-50"
                    },
                    {
                        prioritet: "High",
                        name: "Sber",
                        attributes: "just another attributes",
                        date: "05-05-5005"
                    }
                ]
            }/>
        </div>
    )
}

export default Calendar;