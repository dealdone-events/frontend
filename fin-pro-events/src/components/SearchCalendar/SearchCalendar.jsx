import React from "react";
import styles from "./SearchCalendar.module.css";

const SearchCalendar = () => {
    return(
        <div className={styles.container}>
            <img src="../../icons/search.png" className={styles.searchicon} />
            <input value="Введите" className={styles.searchinput} type=""/>
            <button className={styles.filterbutton}/>
            <label className={styles.date}>ДД.ММ.ГГ-ДД.ММ.ГГ</label>
            <input type="date" className={styles.calendarbutton}/>
        </div>
    )
}

export default SearchCalendar;