import React from "react";
import styles from "./SearchInput.module.css";

const SearchInput = () => {
    return(
        <div>
            <input className={styles.searchinput} ></input>
            <button className={styles.filterbutton} />
        </div>
    )
}

export default SearchInput;