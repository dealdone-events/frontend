import React from "react";
import Calendar from "../Calendar/Calendar";
import Header from "../Header/Header";
import styles from "./CalendarPage.module.css";

const CalendarPage = () => {
    return (
        <div>
            <Header className={styles.header} />
            <div className={styles.calendarpagewrapper}>
                <div/>
                <Calendar className={styles.calendarlist}/>
                <div className={styles.workspacelist}>workspace</div>
            </div>
        </div>
    )
}

export default CalendarPage;